from django.urls import path 

from . import views 

urlpatterns = [ 
    path('', views.UserRegistrationView.as_view(), name='register'),
    path('thank-you/', views.ThankYouView.as_view(), name='thankyou'),

]