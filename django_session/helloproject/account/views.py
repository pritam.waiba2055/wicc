import email
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView
from django.core.mail import send_mail
from django.conf import settings 
from django import forms

from .models import Register
# Create your views here.

class UserRegistrationView(CreateView):
    model = Register
    fields = ["username", "email"]
    template_name = "account/register.html"
    success_url = reverse_lazy('thankyou')

    def get_form(self):
        form = super().get_form()
        for field in form.fields:
            form.fields[field].widget = forms.TextInput(attrs={'class':'form-control'})
            print(field)
        return form

    def form_valid(self, form):
        obj = form.save()
        msg = f'Thank you for the registration {obj.username}.'

        send_mail(
            'WICC training program',
            msg,
            settings.EMAIL_HOST_USER ,
            [obj.email, ]
        )

        return redirect(self.success_url)

class ThankYouView(TemplateView):
    template_name="account/thankyou.html"
    