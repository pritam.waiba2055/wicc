from django.apps import AppConfig


class WiccConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'WICC'
