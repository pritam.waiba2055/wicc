from django.db import models

# Create your models here.
class AboutUsModel(models.Model):
    title = models.CharField(max_length = 100, default='', blank=True, null=True)
    description = models.CharField(max_length = 500, null=False, blank = False)
 
    def __str__(self):
        return self.title

class Article(models.Model):
    title = models.CharField(max_length = 100)
    content = models.TextField()
    date = models.DateField( auto_now = True)

    def __str__(self):
        return self.title 
 