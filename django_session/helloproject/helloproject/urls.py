 
from django.contrib import admin
from django.urls import path, include

from .views import (home_view, about_view, contact_view, article_view, student_view,
    ExpenseListView, AboutUsView, ArticleView
    )

urlpatterns = [
    path('admin/', admin.site.urls),
    path('account', include('account.urls')),

    path('', home_view, name="home"),

    # path('about/', about_view, name="about"),
    path('about/', AboutUsView.as_view(), name="about" ),

    path('contact/', contact_view, name="contact"),

    # path('article/', article_view, name="article"),
    path('article/', ArticleView.as_view(), name="article"),

    path('student/', student_view, name="student"),

    path('expense-list/', ExpenseListView.as_view(), name="expense-list"),


]
