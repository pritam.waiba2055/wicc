from unittest.loader import VALID_MODULE_NAME
from django import forms 
from django.core.exceptions import ValidationError

from . import views
from WICC.models import AboutUsModel, Article

TITLES = ['WICC','Django', 'Python']

"""class AboutUsForm(forms.Form):
    # title = forms.CharField(max_length = 100, required = True, label="Title", widget=forms.TextInput({"class": "form-control", "placeholder": "Title"}))
    # description = forms.CharField(widget = forms.Textarea({"rows":4, "cols": 15, "class": "form-control", "placeholder": "Description"}))

    title = forms.CharField(max_length = 100, required = True, label="Title")
    description = forms.CharField(widget = forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(AboutUsForm, self).__init__(*args, **kwargs)

        # fields = ['title', 'description]
        for field in self.fields:  # self.fields have all the fields attribute above
            self.fields[field].widget.attrs.update({'class': 'form-control', 'placeholder': field.title()})
            
        self.fields['description'].widget.attrs.update({'rows': 5})

    def clean(self):
        cleaned_data = super().clean()
        title = self.cleaned_data.get('title')
        description = self.cleaned_data.get('description') 
        if title in TITLES:
            raise ValidationError("Sorry, the given title already exists.")
        
        if '@' in description:
            raise ValidationError("Sorry, @ is not acceptable in the description")
        
        return self.cleaned_data"""

"""
class ArticleForm(forms.Form):
    title = forms.CharField(max_length = 100)
    content = forms.CharField(widget=forms.Textarea)
    date = forms.DateField(widget = forms.SelectDateWidget())

    def __init__(self, *args, **kwargs):
        super(ArticleForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            if field !='date':
                self.fields[field].widget.attrs.update({'class': 'form-control'})
        
        self.fields['content'].widget.attrs.update({'rows': 6})


    def clean(self):
        cleaned_data = super().clean()
        title = self.cleaned_data.get('title')
        content = self.cleaned_data.get('content')
        date = self.cleaned_data.get('date') 

        if title: # if this is not done, then raises NoneType error when empty field is submitted
            for i in views.article_list:
                if title.lower() == i['title'].lower():
                    raise ValidationError("Title already exist.")
                
        return self.cleaned_data"""

# ModelForm session
class AboutUsForm(forms.ModelForm):

    # as our description is charField, so inorder to make it textarea, we're overriding it
    description = forms.CharField(widget = forms.Textarea) 
    class Meta: 
        model = AboutUsModel
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AboutUsForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Title'})
        self.fields['description'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Description', 'rows':8})
 

class ArticleForm(forms.ModelForm):

    date = forms.DateField(widget = forms.SelectDateWidget())
    class Meta:
        model = Article
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(ArticleForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if field !='date':
                self.fields[field].widget.attrs.update({'class': 'form-control'})
        self.fields['content'].widget.attrs.update({'rows': 6})

    def clean(self):
        cleaned_data = super().clean()
        title = self.cleaned_data.get('title')
        content = self.cleaned_data.get('content')
        date = self.cleaned_data.get('date') 

        if title: # if this is not done, then raises NoneType error when empty field is submitted
            obj = Article.objects.filter(title__iexact = title)
            
            if obj:
                raise ValidationError('Title already exists')
                
        return self.cleaned_data