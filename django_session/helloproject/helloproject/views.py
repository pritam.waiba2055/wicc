from re import template
from typing import List
from django.shortcuts import render, redirect
from django.views.generic import ListView, View, FormView

from .forms import AboutUsForm, ArticleForm
from WICC.models import Article

from faker import Faker
import random 
from datetime import date
fake = Faker()

article_list = [
        {
            "title": "First article",
            "content": "This is the first article",
            "date": "2022-1-2"
        },
        {
            "title": "Second article",
            "content": "This is the Second article",
            "date": "2022-1-3"
        },
        {
            "title": "Third article",
            "content": "This is the Third article",
            "date": "2022-1-4"
        },
        {
            "title": "Fourth article",
            "content": "This is the Fourth article",
            "date": "2022-1-5"
        }
    ]


def home_view(request):
    context = {
        'name':'Nishma Kafle',
        'age': 23,
        'address': 'Gwarko',
        'about_job': {
            'company': 'Infodevelopers',
            'location': 'Sanepa',
        },
        'interests': ['Programming', 'Learning', 'Travelling'],
    }
    return render(request, 'home.html', context)

class AboutUsView(FormView):
    form_class = AboutUsForm
    template_name = "about.html" 
    success_url = "/"

    def form_invalid(self, form):
        print("This form is invalid.")
        print(form.cleaned_data)
        return super().form_invalid(form)

    def form_valid(self, form):
        print("This form is valid.")
        title = form.cleaned_data['title']
        description = form.cleaned_data['description']
        print(title, description, "*"*10)
        form.save()
        form = AboutUsForm()

        # return super().form_valid(form)
        return render(self.request, 'about.html', {'form': form})


def about_view(request): 
    if request.method == "GET": 
        form = AboutUsForm()        
    else:
        form = AboutUsForm(request.POST) 
        if form.is_valid(): 
            title = form.cleaned_data['title']
            description = form.cleaned_data['description']
            print(title, description) 
            form = AboutUsForm()
    return render(request, 'about.html', {'form': form})


def contact_view(request):
    if request.method == "GET": 
        return render(request, 'home.html')
    else:
        email = request.POST.get('email')
        message = request.POST.get('message')
        context = {
            'email': email,
            'message': message
        }
        return render(request, 'contact.html', context)


# Task 2: (form task)

class ArticleView(FormView):
    template_name = "article_list.html" 
    form_class = ArticleForm
    success_url = "/article/"

    def get_context_data(self, **kwargs):
        context =  super().get_context_data(**kwargs)
        # context['article_list'] = article_list
        context['article_list'] = Article.objects.all()
        return context
    
    def form_invalid(self, form):
        print("Article form is invalid.")
        return super().form_invalid(form)

    def form_valid(self, form):
        print("Article form is valid.")

        title = form.cleaned_data.get('title')
        content = form.cleaned_data.get('content')
        article_date = form.cleaned_data.get('date')
        formatted_article_date = article_date.strftime("%Y-%m-%d")
        
        print(title, content, formatted_article_date)

        model = Article(title=title, content=content, date = article_date)
        model.save()
        print(model)
        # saving to the list
        # article_list.append({'title': title, 'content': content, 'date': formatted_article_date})
        
        form = ArticleForm()
        return super().form_valid(form)


def article_view(request):

    if request.method=="POST":
        form = ArticleForm(request.POST)  

        if form.is_valid(): 
            title = form.cleaned_data.get('title')
            content = form.cleaned_data.get('content')
            article_date = form.cleaned_data.get('date')
            formatted_article_date = article_date.strftime("%Y-%m-%d")

            article_list.append({'title': title, 'content': content, 'date': formatted_article_date})
            form = ArticleForm()
    else:
        # converting date of article_list to date  
        """date_splitted = list(map(int, article_list[0]['date'].split('-'))) 
        print(date(date_splitted[0], date_splitted[1], date_splitted[2]))"""

        form = ArticleForm()

    context = {
        'article_list': article_list,
        'form': form
    }
    return render(request, 'article_list.html', context)


# Task1 
def student_view(request):
    student_list = []

    for i in range(1, 11):
        d = dict()
        d['id'] = i
        d['name'] = fake.name()
        d['address'] = fake.address()
        d['email'] = fake.email()
        d['roll'] = i 
        d['mobile'] = '98' +''.join([str(random.randint(0, 9)) for _ in range(8)])     
        student_list.append(d) 
        
    context = {
        'student_list': student_list
    }    
    return render(request, 'student_list.html', context)


# GenericViews
expenses = [
                {
                    'title':'Bread', 
                    'amount': 40
                },
                {
                    'title':'Apple', 
                    'amount': 300
                },
            ]

class ExpenseListView(View):
    
    def get(self, request, *args, **kwargs):
        context = {
            'expenses': expenses,
        } 
        print(self.request.COOKIES)
        return render(request, 'expense_list.html', context)

    def post(self, request, *args, **kwargs):
        title = request.POST.get('title')
        amount = request.POST.get('amount')
        print('Title: ', title)
        print('Amount: ', amount)
        expenses.append({'title': title, 'amount': amount})

        return redirect('/expense-list/')