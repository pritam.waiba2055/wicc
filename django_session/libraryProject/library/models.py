from django.db import models

# Create your models here.

class CommonUser(models.Model):
    name = models.CharField(max_length = 100)
    phone = models.CharField(max_length = 10)
    address = models.CharField(max_length = 100)
    image = models.ImageField(upload_to="librarians")

    class Meta:
        abstract= True

class Librarian(CommonUser):
    salary = models.PositiveIntegerField()
    email = models.EmailField(max_length=255)
    password = models.CharField(max_length = 255)

    def __str__(self):
        return self.name 

class Student(CommonUser): 
    roll = models.PositiveIntegerField() 
    borrowed_book = models.BooleanField(default = False)
    book_count = models.PositiveIntegerField(default = 0)
    fine_amount = models.PositiveIntegerField(default = 0 )

    def __str__(self):
        return self.name 

class Book(models.Model):
    title = models.CharField(max_length = 100)
    genre = models.CharField(max_length = 100)
    author = models.CharField(max_length = 100)
    language = models.CharField(max_length = 100)
    description = models.TextField()
    image = models.ImageField(upload_to="books")
    rating = models.PositiveIntegerField()
    price = models.PositiveIntegerField()
    book_slug = models.SlugField(unique = True, blank=True)

    def __str__(self):
        return self.title 

class Record(models.Model):
    book = models.OneToOneField(Book, primary_key=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    librarian = models.ForeignKey(Librarian, on_delete=models.SET_NULL)
    
    date_of_issue = models.DateField(auto_now_add=True)
    date_of_return = models.DateField(blank = True, null=True)

    def __str__(self):
        return f"{self.student.name}-{self.book.title}-{self.librarian.name}"
