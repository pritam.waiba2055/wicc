from asyncio import constants
from distutils.log import error
from tempfile import tempdir
from typing import List
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import CreateView, DetailView, TemplateView, ListView, DeleteView, UpdateView, View
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.core import serializers
 

from .models import Employee
from .forms import EmployeeForm
# Create your views here.

class EmployeeListView(ListView):
    template_name = "employees/list.html"
    model = Employee
    context_object_name = "employees"
    http_method_names = ['get']

class EmployeeCreateView(CreateView):
    template_name = 'employees/create.html'
    form_class = EmployeeForm
    http_method_names = ['post'] 

    def form_valid(self, form):
        obj = form.save()   
        serialized_obj = serializers.serialize('json', [ obj, ])
        print(type(serialized_obj), serialized_obj)
        return JsonResponse({'status': 200, 'instance': serialized_obj})
        
        # return render(self.request, 'employees/ajax_object.html', {'obj':obj})
    
    def form_invalid(self, form):
        error_list = [(k, v[0]) for k, v in form.errors.items()]
        print(error_list)
        return JsonResponse({'errors': error_list, 'status': 400})


"""def employee_create(request):
    if request.method=="POST":
        form = EmployeeForm(request.POST) 
        if form.is_valid():
            # print(form.cleaned_data)
            obj = form.save()   
            serialized_obj = serializers.serialize('json', [ obj, ])
            print(type(serialized_obj))
            print(serialized_obj)
            return JsonResponse({'status': 200, 'instance': serialized_obj})

        error_list = [(k, v[0]) for k, v in form.errors.items()]
        print(error_list)
        return JsonResponse({'errors': error_list, 'status': 400})
"""
    

class EmployeeDetailView(DetailView):
    template_name = 'employees/detail.html'
    model = Employee
    pk_url_kwarg = 'pk'
    context_object_name = 'employee'
    http_method_names = ['get']

class EmployeeEditView(View):
    
    def get(self, request, *args, **kwargs):
        try:
            employee = Employee.objects.get(id=self.kwargs['pk'])
            serialized_obj = serializers.serialize('json', [employee, ])
            return JsonResponse({'status': 200, 'instance': serialized_obj})
        except Employee.DoesNotExist as e:
            return JsonResponse({'status': 400, 'error': e.__str__()})

    def post(self, request, *args, **kwargs):
        try:
            employee = Employee.objects.get(id = self.kwargs['pk'])
            form = EmployeeForm(request.POST, instance=employee)
            if form.is_valid():
                obj = form.save()
                serialized_obj = serializers.serialize('json', [obj, ])
                return JsonResponse({"status": 200, "instance": serialized_obj})
            else:
                errors = [(k, v[0]) for k, v in form.errors.items() ]
                return JsonResponse({"status": 400, 'errors': errors})
                
        except Employee.DoesNotExist as e:
            return JsonResponse({"status": 400, "errors": e.__str__()})


class EmployeeDeleteView(View):

    def post(self, request, *args, **kawrgs):
        try:
            pk = self.kwargs.get('pk')
            employee = Employee.objects.get(id = pk)
            employee.delete()
            return JsonResponse({'status': 200, 'pk': pk})
        except Employee.DoesNotExist as e:
            print(e.__str__())
            return JsonResponse({'status': 400, 'error': e.__str__()})