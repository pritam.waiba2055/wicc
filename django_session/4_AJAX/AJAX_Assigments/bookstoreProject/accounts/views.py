
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.models import User 
from django.views.generic import CreateView, FormView, View, ListView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy 
from django.contrib import messages
from django.http import Http404
from django.contrib.auth.views import redirect_to_login

from stores.forms import SoldRecordForm
from stores.models import SoldRecord
from .forms import SignUpForm, LoginForm
from stores.models import Book


# Create your views here.

class UserLogoutCheckMixin:

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('home')
        return super(UserLogoutCheckMixin, self).dispatch(request, *args,**kwargs)
         

class UserSignupView(UserLogoutCheckMixin, CreateView):
    form_class = SignUpForm
    template_name = 'accounts/signup.html'
    http_method_names = ['get', 'post']
    success_url = reverse_lazy('login')

class UserLoginView(UserLogoutCheckMixin, FormView):
    form_class = LoginForm
    template_name = 'accounts/login.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        un = form.cleaned_data.get('username')
        pw = form.cleaned_data.get('password')
        user = authenticate(username = un, password=pw) 
        if user:
            login(self.request, user)
        return redirect(self.success_url)

class UserLogoutView(LoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        return redirect('login')

# admin profile 

class UserAccessMixin(PermissionRequiredMixin):

    def dispatch(self, request, *args, **kwargs):
        if (not self.request.user.is_authenticated):
            # get_full_path(): returns 127.0.0.1:8000 path
            # get_login_url(): returns login url
            # get_redirect_field_name(): returns the redirect field name i.e. the next field value 
            return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name() ) 

        if not self.has_permission():
            # return redirect('/')
            raise Http404
            
        return super(UserAccessMixin, self).dispatch(request, *args, **kwargs)


class UserProfileView(UserAccessMixin, ListView):

    raise_exception = False
    permission_required="stores.view_book"

    template_name = 'accounts/admin/adminPanel.html'
    model = Book
    ordering = ['-id']
    context_object_name = 'book_list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sold_record_list'] = SoldRecord.objects.all().order_by('-id')
        return context 
        
class SaleRecordView( UserAccessMixin, CreateView):

    raise_exception = False
    permission_required="stores.add_soldrecord"    

    template_name = 'accounts/admin/saleBook.html'
    form_class = SoldRecordForm
    http_method_names = ['get', 'post'] 
    success_url = reverse_lazy('admin_panel')

    def form_valid(self, form):
        b = form.cleaned_data['book']
        q = form.cleaned_data['sold_quantity']
        t = q * b.price
        b.quantity -= q 
        b.save()
        SoldRecord.objects.create(book=b, sold_quantity=q, total_amount=t)
        messages.success(self.request, 'Books sold successfully!!!')
        return redirect(self.success_url)
 