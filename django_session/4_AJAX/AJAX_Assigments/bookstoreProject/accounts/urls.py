from django.urls import path 

from . import views 

urlpatterns = [ 
    path('signup/', views.UserSignupView.as_view(), name='signup'),
    path('login/', views.UserLoginView.as_view(), name='login'),
    path('logout/', views.UserLogoutView.as_view(), name='logout'),
    path('admin-panel/', views.UserProfileView.as_view(), name='admin_panel'),
    path('sale-book/', views.SaleRecordView.as_view(), name='sale_book'),
    
]