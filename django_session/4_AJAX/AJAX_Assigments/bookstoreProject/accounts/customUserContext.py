from django.contrib.auth.models import Group 

def adminGroupCheck(request):
    result = False 
    if request.user.is_authenticated: 
        if request.user.groups.filter(name='admin-group').exists() or request.user.is_superuser:
            result = True  
    
    return {'panelPermission': result}