from unittest.loader import VALID_MODULE_NAME
from django import forms 
from django.core.exceptions import ValidationError

from .models import Book, SoldRecord

class BookForm(forms.ModelForm):

    class Meta:
        model = Book
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs.update({'class':'form-control'})

        self.fields['description'].widget.attrs.update({'rows':'5'})

class SoldRecordForm(forms.ModelForm):

    class Meta:
        model = SoldRecord
        fields = ['book', 'sold_quantity']
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class':'form-control'})
        
    def clean(self, *args,**kwargs):

        cleaned_data = super().clean(*args,**kwargs)
        book = cleaned_data.get('book')
        sold_quantity = cleaned_data.get('sold_quantity')
        print("-"*10)
        if book and sold_quantity:
            if book.quantity == 0:
                raise ValidationError("Sorry, The book is already sold out.")

            if book.quantity < sold_quantity:
                raise ValidationError(f"Only {book.quantity} books are available but you're trying to sell {sold_quantity} books.")
        