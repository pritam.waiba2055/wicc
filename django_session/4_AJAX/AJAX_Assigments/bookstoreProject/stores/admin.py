from dis import dis
from django.contrib import admin

from .models import Book, Category, SoldRecord
# Register your models here.

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name','slug')
    readonly_fields = ('slug',)

admin.site.register(Book)
admin.site.register(Category, CategoryAdmin)
admin.site.register(SoldRecord)