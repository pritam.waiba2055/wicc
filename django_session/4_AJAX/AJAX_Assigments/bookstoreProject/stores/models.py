from django.db import models
from django.utils.text import slugify

# Create your models here.

# CATEGORY = (
#     ('kids', 'kids'),
#     ('thriller', 'thriller'),
#     ('fiction', 'fiction'),
#     ('history', 'history'),
#     ('politics', 'politics'),
#     ('food', 'food')
# )

class Category(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length = 255, blank=True, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    def __str__(self):
        return self.name 

class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100) 
    category = models.ManyToManyField(Category)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    quantity = models.PositiveIntegerField()
    description = models.TextField()
    image = models.ImageField(upload_to='books')
    added_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.title}"
    
class SoldRecord(models.Model): 
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name='book_sold')
    total_amount = models.DecimalField(max_digits=7, decimal_places=2)
    sold_quantity = models.PositiveIntegerField()  
    sold_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"sold-{self.book}"


    