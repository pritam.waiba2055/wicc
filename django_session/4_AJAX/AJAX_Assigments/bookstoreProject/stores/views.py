from django.shortcuts import render, redirect
from django.http import JsonResponse, Http404
from django.views.generic import ListView, View, DetailView, CreateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.contrib.auth.views import redirect_to_login
from django.contrib.auth.models import Group

from .models import Book, Category
from .forms import BookForm

# Create your views here.


class HomeView(ListView):
    
    template_name = 'stores/home.html'
    model = Book
    ordering = ('-id',)
    http_method_names = ['get']
    context_object_name = 'book_list'

    def get_context_data(self, **kwargs):
         
        context = super().get_context_data(**kwargs)
        context['category_list'] = list(Category.objects.all().values()) 
        return context

class CategorySortBookView(View):
    
    def get(self, request, *args, **kwargs):
        books = list()
        try:
            category_id = self.request.GET.get('category_id')
            category = Category.objects.get(id = category_id)

            for c in category.book_set.all().order_by('-id'):
                d = {
                    'id': c.id, 
                    'title': c.title,
                    'image': c.image.url, 
                    'author': c.author,
                    'description': c.description,
                    'price': c.price,
                }
                books.append(d)
            return JsonResponse({"status": 200, "books": books})
        
        except Exception as e:
 
            for c in Book.objects.all().order_by('-id'):
                d = {
                    'id': c.id, 
                    'title': c.title,
                    'image': c.image.url, 
                    'author': c.author,
                    'description': c.description,
                    'price': c.price,
                }
                books.append(d)
            return JsonResponse({"status": 400, "books": books})
 

class BookDetailView(DetailView):
    template_name = 'stores/detail.html'
    model = Book 
    context_object_name = 'book'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        category = ""
        for c in self.get_object().category.all():
           category += c.name + ", "
        context['category'] = category
        return context


class BookSortView(View):
    
    def get(self, request, *args, **kwargs):
        books = list()
        try:
            sort_by = self.request.GET.get('sort_by') 
            for c in Book.objects.all().order_by(sort_by):
                d = {
                    'id': c.id, 
                    'title': c.title,
                    'image': c.image.url, 
                    'author': c.author,
                    'description': c.description,
                    'price': c.price,
                }
                books.append(d) 
            return JsonResponse({"status": 200, "books": books})
        
        except Exception as e: 

            for c in Book.objects.all().order_by('-id'):
                d = {
                    'id': c.id, 
                    'title': c.title,
                    'image': c.image.url, 
                    'author': c.author,
                    'description': c.description,
                    'price': c.price,
                }
                books.append(d)
            return JsonResponse({"status": 400, "books": books})
 
class BookSearchView(View):

    def get(self, request, *args, **kwargs):

        keyword = request.GET.get('keyword') 
        try:
            books = Book.objects.filter(author__icontains=keyword) 
        except Exception as e: 
            books =  Book.objects.all().order_by('-id')

        context = {
            'book_list': books,
            'keyword': keyword
        }
        # return JsonResponse({"status": 400, "books": books})
        return render(request, 'stores/search.html', context)


"""class SuperUserCheckMixin(UserPassesTestMixin):
 
    def test_func(self):
        return self.request.user.is_superuser"""

class UserAccessMixin(PermissionRequiredMixin):

    def dispatch(self, request, *args, **kwargs):
        if (not self.request.user.is_authenticated):
            # get_full_path(): returns 127.0.0.1:8000 path
            # get_login_url(): returns login url
            # get_redirect_field_name(): returns the redirect field name i.e. the next field value 
            return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name() ) 

        if not self.has_permission():
            # return redirect('/')
            raise Http404
            
        return super(UserAccessMixin, self).dispatch(request, *args, **kwargs)


class BookCreateView(UserAccessMixin, CreateView):

    raise_exception = False
    permission_required = "stores.add_book"
    permission_denied_message = 'Hacker Vai' 

    form_class = BookForm
    model = Book
    template_name = 'stores/create.html'
    context_object_name = 'form'
    success_url = reverse_lazy('home')


    # def form_valid(self, form):
    #     print("form is valid")
    #     print(form.cleaned_data)
    #     return super().form_valid(form)

    # def form_invalid(self, form):
    #     print("form invalid")
    #     print(form.errors.items())

    #     return super().form_invalid(form)