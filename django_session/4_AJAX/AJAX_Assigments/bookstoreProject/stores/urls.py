from django.urls import path 

from . import views 

urlpatterns = [ 
    path('', views.HomeView.as_view(), name='home'),
    path('create/', views.BookCreateView.as_view(), name='create'),
    path('category-sort/', views.CategorySortBookView.as_view(), name='category_sort'),
    path('book-sort/', views.BookSortView.as_view(), name='book_sort'),
    path('book-search/', views.BookSearchView.as_view(), name='book_search'),
    path('<int:pk>/book-detail/', views.BookDetailView.as_view(), name='book_detail'),

    

    
]