from sre_constants import SUCCESS
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.generic import View, ListView, DeleteView, DetailView, UpdateView, CreateView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core import serializers

from .models import BlogPost, ImageModel,Comment
from .forms import BlogPostForm, CommentForm, BlogUpdateForm
# Create your views here.

class BlogPostListView(ListView):
    model = BlogPost
    ordering = ('-id',)
    template_name = 'blog/list.html'
    context_object_name = 'post_list'
    http_method_names = ['get',]


class BlogPostDetailView(DetailView):
    model =BlogPost
    http_method_names = ['get']
    template_name = 'blog/detail.html'
    context_object_name = 'post'

class BlogPostCreateView(LoginRequiredMixin, View):
    
    def get(self, request, *args, **kwargs): 
        form = BlogPostForm()
        context = {'form':form}
        return render(request, 'blog/create.html', context)

    def post(self, request, *args, **kwargs):
        form = BlogPostForm(request.POST, request.FILES) 
        if form.is_valid(): 
            obj = BlogPost.objects.create(title = request.POST.get('title'), content=request.POST.get('content'), user = self.request.user )
 
            supported_image_format = ['jpg','jpeg','png'] 
            for img in request.FILES.getlist('image'):
                if img.name.split('.')[-1].lower() in supported_image_format:
                    img = ImageModel.objects.create(post = obj, image = img) 
            
            return redirect('blog_list')
                
        form = BlogPostForm()
        context = {'form':form}
        return render(request, 'blog/create.html', context)

class CommentView( LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs): 
        try:
            form = CommentForm(request.POST)
            print(form.is_valid())
            if form.is_valid():
                p = BlogPost.objects.get(id = self.kwargs.get('pk')) 
                comment = Comment.objects.create(post=p, user=request.user, content=form.cleaned_data['content'])
                print(comment) 
                instance_obj = {
                    'user': request.user.username, 
                    'content': form.cleaned_data['content'],
                    'date_time': comment.date_time.strftime("%b. %d, %Y, %I:%M %P").replace('am', 'a.m.').replace('pm', 'p.m.')
                }
                print(instance_obj)
                return JsonResponse({'status': 200, 'instance': instance_obj})
            else:
                errors = [(index, value[0]) for index,value in form.errors.items()]
                print(errors)
                return JsonResponse({'status': 400, 'errors': errors})

        except BlogPost.DoesNotExist as e:
            print(e)

class BlogPostUpdateView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        blog = BlogPost.objects.get(id = self.kwargs.get('pk'))
        # blog = BlogPost.objects.get(id = 33434)
        print(blog) 
        instance_obj = {
            'content': blog.content,
            'title': blog.title,
            'id': blog.id
        }
        return JsonResponse({'status': 200, 'instance': instance_obj})

    def post(self, requets,*args, **kwargs):
        form = BlogUpdateForm(self.request.POST)
        if form.is_valid():
            post = BlogPost.objects.get(id = self.kwargs.get('pk'))
            post.title = form.cleaned_data.get('title')
            post.content = form.cleaned_data.get('content')
            post.save()
            instance = {
                'title': post.title,
                'content': post.content,
                'user': self.request.user.username,
                'updated': post.updated.strftime("%b. %d, %Y, %I:%M %P").replace('am', 'a.m.').replace('pm', 'p.m.')
            }
            return JsonResponse({'status': 200, 'instance': instance})
        else:
            errors = [(key, value[0]) for key,value in form.errors.items()]
            print(errors)
            return JsonResponse({'status': 400, 'errors': errors})


class BlogDeleteView(LoginRequiredMixin, DeleteView):
    http_method_names = ['post']
    model = BlogPost
    success_url = reverse_lazy('blog_list')
     
    def form_valid(self, form): 
        self.object.delete()
        return JsonResponse({'status': 200})
