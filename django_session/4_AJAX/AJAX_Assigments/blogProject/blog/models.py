  
from django.db import models
from django.utils.text import slugify
from django.urls import reverse  
from django.contrib.auth.models import User 
# Create your models here.


class BlogPost(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_post')
    title = models.CharField(max_length = 100, null=False, blank=False)
    content = models.TextField()
    slug = models.SlugField(max_length = 255, null=True, blank=True) 

    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(BlogPost, self).save(*args, **kwargs)

    def get_absolute_url(self): 
        return reverse('blog_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title 

# def validate_file_extension(value):
#     supported_image_format = ['jpg','jpeg','png'] 
#     if value.split('.')[-1] not in supported_image_format:
#         raise ValidationError("error adsfadfas")

class ImageModel(models.Model):
    post = models.ForeignKey(BlogPost, on_delete=models.CASCADE, related_name="post_image")
    image = models.ImageField(upload_to='images', null=True, blank=True)

class Comment(models.Model):
    post = models.ForeignKey(BlogPost, on_delete=models.CASCADE, related_name='post_comment')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_comment')
    content = models.TextField(null=False, blank=False)
    date_time = models.DateTimeField(auto_now_add=True)
    
    

