from django.urls import path 

from . import views 

urlpatterns = [ 
    path('', views.BlogPostListView.as_view(), name='blog_list'),
    path('create/', views.BlogPostCreateView.as_view(), name='blog_create'),
    path('<int:pk>/detail/', views.BlogPostDetailView.as_view(), name='blog_detail'),
    path('<int:pk>/comment/', views.CommentView.as_view(), name='blog_comment'),
    path('<int:pk>/update/',views.BlogPostUpdateView.as_view(), name='blog_update'),
    path('<int:pk>/delete/', views.BlogDeleteView.as_view(), name='blog_delete'),
    
]