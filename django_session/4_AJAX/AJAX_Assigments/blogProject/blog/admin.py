from django.contrib import admin

from .models import BlogPost, ImageModel, Comment
# Register your models here.

class BlogPostAdmin(admin.ModelAdmin):
    list_display = ('title','slug', 'created')
    readonly_fields = ('slug',)

admin.site.register(BlogPost, BlogPostAdmin)

admin.site.register(ImageModel)
admin.site.register(Comment)