from django import forms
from django.core.exceptions import ValidationError 

from .models import BlogPost, Comment

class BlogPostForm(forms.ModelForm):

    image = forms.ImageField(widget = forms.FileInput(attrs={'multiple': True,"accept": "image/*", "required":False}))

    class Meta:
        model = BlogPost
        exclude = ('slug','user' )

    def __init__(self, *args, **kwargs):
        super(BlogPostForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
    
    # def clean_image(self):
    #     img = self.cleaned_data['image']
    #     print("-"*10)
    #     print(img)
    #     supported_image_format = ['jpg','jpeg','png'] 
    #     if img:
    #         if img.name.split('.')[-1].lower() not in supported_image_format:
    #             print('validation err')
    #             raise ValidationError("['jpg','jpeg','png'] are supoorted image formats.")
        
    #     return img
        

class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('content',)

class BlogUpdateForm(forms.ModelForm):

    class Meta:
        model = BlogPost
        fields = ('title', 'content')