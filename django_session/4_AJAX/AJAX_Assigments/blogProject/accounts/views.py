from typing import List
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.models import User 
from django.views.generic import CreateView, FormView, View, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .forms import SignUpForm, LoginForm
from blog.models import BlogPost

# Create your views here.

class UserLogoutCheckMixin:

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('blog_list')
        return super(UserLogoutCheckMixin, self).dispatch(request, *args,**kwargs)
         

class UserSignupView(UserLogoutCheckMixin, CreateView):
    form_class = SignUpForm
    template_name = 'accounts/signup.html'
    http_method_names = ['get', 'post']
    success_url = reverse_lazy('login')

class UserLoginView(UserLogoutCheckMixin, FormView):
    form_class = LoginForm
    template_name = 'accounts/login.html'
    success_url = reverse_lazy('blog_list')

    def form_valid(self, form):
        un = form.cleaned_data.get('username')
        pw = form.cleaned_data.get('password')
        user = authenticate(username = un, password=pw) 
        if user:
            login(self.request, user)
        return redirect(self.success_url)

class UserLogoutView(LoginRequiredMixin, View):
    def get(self, request):
        logout(request)
        return redirect('login')

class UserProfileView(LoginRequiredMixin, ListView):
    template_name = 'accounts/profile.html'
    model = BlogPost
    context_object_name = 'post_list'

    def get_queryset(self):
        return self.request.user.user_post.all().order_by('-id')
