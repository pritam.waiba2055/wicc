
from enum import unique
from django.db import models

# Create your models here.

GENDER = (('MALE', 'MALE'), ('FEMALE','FEMALE'))

class Zone(models.Model):
    name = models.CharField(max_length = 255, unique=True)

    def __str__(self):
        return self.name 

class District(models.Model):
    zone = models.ForeignKey(Zone, on_delete=models.CASCADE)
    name = models.CharField(max_length = 255, unique=True)

    def __str__(self):
        return f"{self.zone.name} -> {self.name}" 

class Student(models.Model):
    name = models.CharField(max_length=255)
    age = models.PositiveIntegerField()
    gender = models.CharField(max_length=10, choices=GENDER)
    zone = models.ForeignKey(Zone, on_delete=models.CASCADE)
    district = models.ForeignKey(District, on_delete=models.CASCADE)

    def __str__(self):
        return self.name 
