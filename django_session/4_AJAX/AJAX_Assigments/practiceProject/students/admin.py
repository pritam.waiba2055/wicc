from os import ST_APPEND
from django.contrib import admin

from .models import Zone, Student, District
# Register your models here.

admin.site.register(Zone)
admin.site.register(Student)
admin.site.register(District)