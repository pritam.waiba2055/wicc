from django.urls import path 

from . import views 

urlpatterns = [ 
    path('', views.IndexView.as_view(), name='index'),
    path('<int:zone_id>/get-district/', views.GetDistrictAjaxView.as_view(), name='get_district'),
    
]