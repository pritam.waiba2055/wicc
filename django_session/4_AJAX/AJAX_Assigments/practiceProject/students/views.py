from django.shortcuts import render
from django.views.generic import View
from django.http import JsonResponse

import csv

from .models import District, Zone, Student
from .forms import StudentForm
# Create your views here.


class IndexView(View):
    
    def get(self, request, *args, **kwargs):

        # saving data to database
        """file = open('data/zones_with_districts.csv')
        f = csv.reader(file)
        print(next(f)) 
        zone = set()
        district = set()

        for row in f:
            if row[0] not in zone:
                zone.add(row[0])
                Zone.objects.create(name=row[0])

            if row[1] not in district:
                district.add(row[1])
                zone_obj = Zone.objects.get(name=row[0])
                District.objects.create(name=row[1], zone= zone_obj)"""

        print(Zone.objects.count())
        print(District.objects.count())

        context = {
            'form': StudentForm
        }
        
        return render(request, 'students/index.html', context)

class GetDistrictAjaxView(View):

    def get(self, request, *args, **kwargs): 
        district = District.objects.filter(zone = self.kwargs.get('zone_id') )
        # print(district)
        # print( list(district.values()) )
        return JsonResponse({'status': 200, 'data': list(district.values('id', 'name'))})