from django import forms 
from django.core.exceptions import ValidationError

from .models import Employee

class EmployeeForm(forms.ModelForm):

    class Meta:
        model = Employee
        fields = '__all__'
    
    def __init__(self, *args, **kwargs): 
        super().__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control', 'placeholder': field.title()})

    def clean(self, *args, **kwargs):
        cleaned_data = super().clean(*args, **kwargs)
        # print("-"*10)
        # print(cleaned_data)

        age = cleaned_data.get('age')
        if age:
            if age < 16:
                # raise ValidationError('You\'r under age.')
                self.add_error('age', 'You\'r under age.')