from django import forms 

from .models import Employee

class EmployeeForm(forms.ModelForm):

    class Meta:
        model = Employee
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        print(kwargs)
        user = kwargs.pop('user')
        print(user)  
        print(kwargs)
        kwargs.pop('initial')
        super().__init__(*args, **kwargs)  
        
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control', 'placeholder': field.title()})
