from tempfile import tempdir
from typing import List
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import CreateView, DetailView, TemplateView, ListView, DeleteView, UpdateView
from django.urls import reverse_lazy

from .models import Employee
from .forms import EmployeeForm
# Create your views here.

class EmployeeListView(ListView):
    template_name = "employee/list.html"
    model = Employee
    context_object_name = "employees"
    http_method_names = ['get']

class EmployeeCreateView(CreateView):
    template_name = 'employee/create.html'
    form_class = EmployeeForm
    http_method_names = ['get','post']
    success_url = reverse_lazy('employee_list')
    initial = {
        'name': 'Gopal',
    }

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs() 
        kwargs['user'] = self.request.user 
        return kwargs

class EmployeeDetailView(DetailView):
    template_name = 'employee/detail.html'
    model = Employee
    pk_url_kwarg = 'pk'
    context_object_name = 'employee'
    http_method_names = ['get']

class EmployeeEditView(UpdateView):
    template_name = 'employee/edit.html'
    pk_url_kwarg = 'pk'
    form_class = EmployeeForm
    model= Employee
    http_method_names = ['get', 'post']
    success_url = reverse_lazy('employee_list')

class EmployeeDeleteView(DeleteView):
    template_name = 'employee/delete.html'
    pk_url_kwarg = 'pk'
    model = Employee
    http_method_names = ['get', 'post']
    success_url = reverse_lazy('employee_list')
