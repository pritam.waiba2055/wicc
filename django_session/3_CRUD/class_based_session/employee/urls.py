from django.urls import path 

from . import views 

urlpatterns =  [
    path('', views.EmployeeListView.as_view(), name='employee_list' ),
    path('create/', views.EmployeeCreateView.as_view(), name='employee_create'),
    path('detail/<int:pk>/', views.EmployeeDetailView.as_view(), name='employee_detail'),
    path('edit/<int:pk>/', views.EmployeeEditView.as_view(), name='employee_edit'),
    path('delete/<int:pk>/', views.EmployeeDeleteView.as_view(), name='employee_delete'),
    
]