from django.db import models

# Create your models here.

class Employee(models.Model):

    name = models.CharField(max_length = 255,blank = False, null = False)
    email = models.EmailField()
    contact = models.CharField(max_length = 10, unique=True, blank=False, null=False)

    def __str__(self):
        return self.name 