from django.db import models

# Create your models here.

class Employee(models.Model):

    emp_name = models.CharField(max_length = 255,blank = False, null = False)
    emp_email = models.EmailField()
    emp_contact = models.CharField(max_length = 10, unique=True, blank=False, null=False)

    def __str__(self):
        return self.emp_name 
  
