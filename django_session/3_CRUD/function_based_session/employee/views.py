from django.shortcuts import render, redirect, get_object_or_404

from .models import Employee
from .forms import EmployeeForm

# Create your views here.

def employee_list(request):

    employees = Employee.objects.all()
    context = { 
        'employees': employees
    }
    return render(request, 'employee/list.html', context )


def create_employee(request):
    
    form = EmployeeForm()

    if request.method == 'POST':
        form = EmployeeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('employee_list') 

    context = {
        'form': form 
    }
    return render(request, 'employee/create.html', context)

def edit_employee(request, pk):

    employee = Employee.objects.get(id = pk )
    
    if request.method == 'POST':
        employee_form = EmployeeForm(request.POST, instance=employee)
        if employee_form.is_valid():
            employee = employee_form.save(commit)
            employee_form.save()
            return redirect('employee_list')

    else:
        employee_form = EmployeeForm(instance = employee)

    context = {
        'form': employee_form
    }
    return render(request, 'employee/edit.html', context)

def employee_detail(request, pk ):
    
    employee = Employee.objects.get(id =pk )
    context = {
        'employee': employee 
    }
    return render(request, 'employee/detail.html', context)


def delete_employee(request, pk): 
    
    employee = get_object_or_404(Employee, id=pk)

    if request.method == 'POST':
        employee.delete()
        return redirect('employee_list')
    
    context = {
        'employee': employee
    }
    return render(request, 'employee/delete.html', context)
    