from django.urls import path 

from . import views 

urlpatterns = [ 
    path('', views.employee_list, name='employee_list'),
    path('create', views.create_employee, name='employee_create'),
    path('edit/<pk>', views.edit_employee, name='employee_edit'),
    path('detail/<pk>', views.employee_detail, name='employee_detail'),
    path('delete/<pk>', views.delete_employee, name='employee_delete'),
     
]