from django import forms 
from django.db import transaction

from .models import Employee

class EmployeeForm(forms.ModelForm):

    class Meta:
        model = Employee
        fields = '__all__'

    # we can also do this in views.py file and they are same
    # this behavior will affect for both add and update method
    """@transaction.atomic
    def save(self, commit = True):
        employee = super().save(commit = False)
        employee.emp_name = self.cleaned_data.get('emp_name') + '001'
        employee.save()"""