from django.db import models

# Create your models here.

class Student(models.Model):

    name = models.CharField(max_length = 255,blank = False, null = False)
    age = models.PositiveIntegerField()
    gender =models.CharField(max_length = 20)

    def __str__(self):
        return self.name 
  
   