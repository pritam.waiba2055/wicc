from tempfile import tempdir
from typing import List
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import CreateView, DetailView, TemplateView, ListView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin

from .models import Student
from .forms import StudentForm


# Create your views here.

class StudentListView(ListView):

    # queryset = Student.objects.all()

    template_name = "students/list.html"
    model = Student
    context_object_name = "students"
    paginate_by = 2
    http_method_names = ['get']
    ordering = '-id'
    allow_empty = True 

    def get_queryset(self):

        return super().get_queryset()

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context['date'] = '2022-02-08'
        return context 


class StudentCreateView(SuccessMessageMixin, CreateView):
    template_name = 'students/create.html'
    model = Student 
    form_class = StudentForm
    success_url = reverse_lazy('student_list')
    http_method_names = ['get', 'post']

    # initial = {
    #     'name': 'hari',
    #     'age': 3,
    #     'gender': 'male'
    # }

    success_message = "student has been created successfully"

    # override success_url 
    """def get_success_url(self):
        # return super().get_success_url()
        return reverse('')"""

    def get_form_class(self): # called while sending form in url
        return StudentForm

    def get_form(self, form_class = None):
        form = super().get_form()

    # is called on both GET and POST request when Form is send or received
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['users'] = self.request.user 
        return kwargs

    def form_valid(self, form):
        return super().form_valid(form)

class StudentDetailView(DetailView):
    template_name = 'students/detail.html'
    model = Student
    pk_url_kwarg = 'pk'
    context_object_name = 'student'
    # content_type = 'text/html' # by default and no need to override


class StudentUpdateView(SuccessMessageMixin, UpdateView):
    template_name = 'students/edit.html'
    pk_url_kwarg = 'pk'
    form_class = StudentForm
    model= Student
    success_url = reverse_lazy('student_list')

    # def get_form_class(self, request): # called while sending form in url
    #     if request.user.is_authenticated:
    #         return StudentForm
    #     else:
    #         return AddressForm

class StudentDeleteView( SuccessMessageMixin, DeleteView):
    template_name = 'studnets/delete.html'
    pk_url_kwarg = 'pk'
    model = Student
    success_url = reverse_lazy('student_list')

    # django send empty form by default while deleteing, and that form is always valid
    # this is how methods are called while deleting 
    """def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            self.form_valid(form)

    def form_valid(self, form):
        self.objects.delete()
        return super().form_valid(form)"""  