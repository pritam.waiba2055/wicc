
import re 
pattern = re.compile(r'^[a-zA-Z]{1}$')

def hangman(word, hint, mistake_limit):

    correct_word = list(word)
    word_len = len(correct_word)

    word_char_index = {}
    for index, value in enumerate(correct_word):

        if value not in word_char_index:
            word_char_index[value] = [index]
        else:
            word_char_index[value].append(index)
 
    mistake_counter = 0
    correct_counter = 0 
    guessed_word = ["_" for _ in range(word_len) ]  

    print("\n\t\t\"Hangman Game Started\"")
    print(f"\n\t\tHint: {hint}")    
    while correct_counter < word_len and mistake_counter < mistake_limit:

        print("\n", "-"*50)
        print(f"\nmistake_count: {mistake_counter}, correct_count: {correct_counter}, Guessed word: {' '.join([i for i in guessed_word])}")
        guessed_char = input("Enter a guess character: ").strip().lower()

        # checking guessed_char if it's length is more than 1 and is alphabet or not using regex
        if re.match(pattern, guessed_char) is None:
            print('\t !!! Warning: Enter a character of length 1 and it must be alphabet !!!')
        else:
            if guessed_char in correct_word:
                if guessed_char not in guessed_word:

                    for index in word_char_index[guessed_char]:
                        guessed_word[index] = guessed_char
                        correct_counter+=1
            else:
                mistake_counter += 1

    print("\n")
    print("*"*37)
    print("Mistake count: ", mistake_counter, ", Guessed count: ", correct_counter)
    print('Guessed word: ', ' '.join([i for i in guessed_word]) )
    print('Correct Word: ', ' '.join([i for i in correct_word]) )

    if correct_counter != word_len:
        print(f'\nYou hit the total mistake limit {mistake_limit}')
        print('Sorry, you lose the game.')
        print("Now, you're hanged.")
        print()
        print('      _________')
        print('     |        |')
        print('     O        |')
        print('    /|\\       |')
        print('     |        |')
        print('    / \\       |')
        print('              |')
        print('              |')
        print('            __|__')
        print() 
    else:
        print('\nCongratulations!, You won the game.')


if __name__=="__main__":

    word = "apple"
    hint = "It's a fruit."
    mistake_limit = 7
    hangman(word, hint, mistake_limit)