
# Task 1
"""def checkInList(a, val):
    result = False
    for i in a:
        if val == i:
            result = True
            break
        
    if not result:
        print("NOt present")
    else:
        print("Present")

a = [2,4,6,8,9]
val = int(input('Enter value: '))
checkInList(a, val)"""

# Task 2 

def histogram(word):
    result = dict()
    for i in word:
        if i not in result:
            result[i] = 1
        else:
            result[i] += 1
    return result

list_of_fruits = ['apple', 'orange', 'grapes', 'apple', 'apple', 'grapes'] 
string_value = "apple"
tuple_value = ('span', 'egg', 'spam', 'spam', 'bacon', 'spam')

print(histogram(list_of_fruits))
print(histogram(string_value))
print(histogram(tuple_value))