
class Time():
    """ 
    Time class 
    hour, minute, second
    """
    def __init__(self, hour = 0, minute=0, second=0):
        self.hour = hour 
        self.minute = minute    
        self.second = second
    
    def __str__(self):
        return f"{self.hour} : {self.minute} : {self.second}"

    def add_time(self, t2): 

        s_quotient = (self.second + t2.second) // 60
        self.second = (self.second + t2.second) % 60
        
        m_quotient = (self.minute + t2.minute + s_quotient) // 60
        self.minute = (self.minute + t2.minute + s_quotient) % 60
        
        self.hour = self.hour + t2.hour + m_quotient
        
        print(self)

start = Time(9, 45, 0) 
duration = Time(1, 35, 0) 

start.add_time(duration)
 