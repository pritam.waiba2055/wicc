  
class Vehicle:

    def __init__(self, company, color, model, no_of_wheels):
        self.company = company
        self.color = color 
        self.model = model
        self.no_of_wheels = no_of_wheels

    def __str__(self):
        return self.company +" " +  self.model

    def horn(self):
        print("Peep! Peep!")

    def change_tyre(self):
        print("Changing tyre")

    def open_door(self):
        print("Door opened")

    def close_door(self):
        print("Door closed")

    def repair(self):
        print("Repairing vehicle")

    def change_gear(self):
        print("Gear changed automatically.")

class Bus(Vehicle):
    bus_count = 0
    
    def __init__(self, company, color, model, no_of_wheels, carrying_capacity):
        super().__init__(company, color, model, no_of_wheels)
        self.carrying_capacity = carrying_capacity
        self.passenger_count = 0
        Bus.bus_count += 1

    def add_passenger(self, n):
        if self.passenger_count + n  > self.carrying_capacity:
            print(f"\tWarning, passenger capacity is only {self.carrying_capacity}. But you're trying to accomodate {self.passenger_count + n} passengers.")
        else:
            self.passenger_count += n

    def drop_passenger(self, n):
        if self.passenger_count == 0 or self.passenger_count-n <=0 :
            print(f'No passenger in the bus avaibale or {n} no. of passengers not available in the bus.')
        else:
            self.passenger_count -= n
            print(f'{n} passengers dropped and available passengers: {self.passenger_count}')

    def __str__(self):
        r = f"\n(Bus Description)\nCompany: {self.company}, Model: {self.model}, Color: {self.color}, No_of_wheels: {self.no_of_wheels}"
        r += f"\nCarrying_capacity: {self.carrying_capacity}, Passenger_count: {self.passenger_count}"
        return r 

"""
class Car(Vehicle):
    
    def __init__(self, company, color, model, no_of_wheels, carrying_capacity):
        super().__init__(company, color, model, no_of_wheels)
        self.carrying_capacity = carrying_capacity"""


bus1 = Bus('TATA', 'White', 'X', 4, 50)
bus1.add_passenger(4)
bus1.add_passenger(8)
bus1.add_passenger(40)
print(bus1)

bus1.drop_passenger(3)
print(bus1)

# bus2 = Bus('Mahindra', 'GREEN', 'YY', 4, 30)
# bus2.add_passenger(4)
# bus2.add_passenger(10)
# print(bus2)

