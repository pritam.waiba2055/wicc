from django.shortcuts import render

from rest_framework.generics import CreateAPIView
from rest_framework import viewsets

from .serializers import BookSerializer, AuthorSerializer
from .models import Author 
# Create your views here.

class BookCreateView(CreateAPIView):

    serializer_class = BookSerializer

class AuthorViewSet(viewsets.ModelViewSet):
    serialier_class = AuthorSerializer
    queryset = Author.objects.all()
