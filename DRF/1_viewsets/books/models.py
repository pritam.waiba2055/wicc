from django.db import models
 
from django.utils.text import slugify
# Create your models here.

class TimeStampedAuthModel(models.Model):
    created_date = models.DateTimeField(blank = True, auto_now_add=True)
    updated_date = models.DateTimeField(blank = True, auto_now=True)

    class Meta:
        abstract = True 

class Author(TimeStampedAuthModel):
    first_name = models.CharField(max_length = 100)
    last_name = models.CharField(max_length = 100)
    email = models.EmailField(blank=True, null=True)

class Book(TimeStampedAuthModel):
    name = models.CharField(max_length=100)
    slug = models.SlugField()
    author = models.ManyToManyField(Author, related_name='book_author')
    published_date = models.DateTimeField(null=True)
    language = models.CharField(max_length=100, default='English')
     

    def save(self, *args, **kwargs):

        self.slug = slugify(self.name)
        return super().save()