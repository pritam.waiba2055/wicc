from rest_framework import serializers 

from .models import Book, Author

class BookSerializer(serializers.ModelSerializer ):

    class Meta:
        model = Book
        fields = ('name',)

class AuthorSerializer(serializers.ModelSerializer):

    class Meta:
        mode = Author 
        fields = '__all__'