from django.contrib import admin

from .models import Author, Book
# Register your models here.

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ['name']

# admin.site.register(Book)
admin.site.register(Author)