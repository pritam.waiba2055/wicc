from django.shortcuts import get_object_or_404 

from rest_framework import viewsets, status
from rest_framework.response import Response

from .models import ParkingRecord, TicketCounter
from .serializers import ParkingRecordSerialiser, TicketCounterSerializer
# Create your views here.

class ParkingRecordViewSets(viewsets.ViewSet):

    def list(self, request):
        queryset = ParkingRecord.objects.all() 
        serializer = ParkingRecordSerialiser(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serialized = ParkingRecordSerialiser(data = request.data)

        if serialized.is_valid(): 
            serialized.save()
            return Response({'status': status.HTTP_200_OK, 'data': serialized.data})

        return Response({'status':status.HTTP_400_BAD_REQUEST , 'errors' : serialized.errors})

    def retrieve(self, request, pk=None): 
        obj = get_object_or_404(ParkingRecord, id=pk) 
        serialized = ParkingRecordSerialiser(obj)
        return Response(serialized.data)

    def update(self, request, pk=None):
        obj = get_object_or_404(ParkingRecord, id=pk)
        serialized = ParkingRecordSerialiser(data=request.data, instance=obj)
        if serialized.is_valid():
            serialized.save()
            return Response({'status': status.HTTP_200_OK, 'updated_data': serialized.data})
        return Response({'status': status.HTTP_400_BAD_REQUEST, 'errors': serialized.errors})
    
    def partial_update(self, request, pk=None):
        obj = get_object_or_404(ParkingRecord, id=pk)
        serialized = ParkingRecordSerialiser(data=request.data, instance=obj, partial=True)
        if serialized.is_valid():   
            serialized.save()
            return Response({'status': status.HTTP_200_OK, 'partial_updated_data': serialized.data})
        return Response({'status': status.HTTP_400_BAD_REQUEST, 'errors': serialized.errors})

    def destroy(self, request, pk=None):
        obj = get_object_or_404(ParkingRecord, id=pk)
        obj.delete()
        return Response({'status': status.HTTP_200_OK, 'message': 'Deletion success'})
        