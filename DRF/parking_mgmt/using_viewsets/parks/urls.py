from django.urls import path, include 
from rest_framework.routers import DefaultRouter

from . import views  

router = DefaultRouter()
router.register('parking', views.ParkingRecordViewSets, basename='parking')

urlpatterns = [  
    path('', include(router.urls)),

] 