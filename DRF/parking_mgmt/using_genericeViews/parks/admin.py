from django.contrib import admin

from .models import TicketCounter, ParkingRecord
# Register your models here.

@admin.register(TicketCounter)
class TicketCounterAdmin(admin.ModelAdmin):
    list_display = ['ticket_seller_name','date_time']


@admin.register(ParkingRecord)
class ParkingRecordAdmin(admin.ModelAdmin):
    list_display = ['driver_name','vehicle_name', 'arrival_time','departure_time','paid']

