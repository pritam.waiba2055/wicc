from django.db import models

# Create your models here.

class TicketCounter(models.Model):
    ticket_seller_name = models.CharField(max_length=100)
    date_time = models.DateTimeField(auto_now_add = True )

    def __str__(self):
        return self.ticket_seller_name

class ParkingRecord(models.Model):

    ticket_counter = models.ForeignKey(TicketCounter, on_delete=models.CASCADE)
    driver_name = models.CharField(max_length=100)
    vehicle_name = models.CharField(max_length=100)
    arrival_time = models.DateTimeField(auto_now_add=True)
    departure_time = models.DateTimeField(null=True, blank=True)
    paid = models.BooleanField(default=False)

    def __str__(self):
        return self.driver_name