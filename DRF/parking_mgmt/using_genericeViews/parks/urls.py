from django.urls import path 

from . import views 

urlpatterns = [ 
    path('', views.ParkingRecordListView.as_view(), name='list'),
    path('create/', views.ParkingRecordCreateView.as_view(), name='create'),
    path('<int:pk>/', views.ParkingRecordRetrieveView.as_view(), name='retrieve'),
    path('<int:pk>/update/', views.ParkingRecordUpdateView.as_view(),name='update'),
    path('<int:pk>/destory/', views.ParkingRecordDestroyView.as_view(), name='destroy'), 

]