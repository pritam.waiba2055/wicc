from django.shortcuts import render

from rest_framework.generics import CreateAPIView, ListAPIView,RetrieveAPIView, UpdateAPIView, DestroyAPIView

from .models import ParkingRecord, TicketCounter
from .serializers import ParkingRecordSerialiser, TicketCounterSerializer
# Create your views here.

class ParkingRecordListView(ListAPIView):
    queryset = ParkingRecord.objects.all()
    serializer_class = ParkingRecordSerialiser

class ParkingRecordCreateView(CreateAPIView):
    queryset = ParkingRecord.objects.all()
    serializer_class = ParkingRecordSerialiser

class ParkingRecordRetrieveView(RetrieveAPIView):
    queryset = ParkingRecord.objects.all()
    serializer_class = ParkingRecordSerialiser

class ParkingRecordUpdateView(UpdateAPIView):
    queryset = ParkingRecord.objects.all()
    serializer_class = ParkingRecordSerialiser

class ParkingRecordDestroyView(DestroyAPIView):
    queryset = ParkingRecord.objects.all()
    serializer_class = ParkingRecordSerialiser