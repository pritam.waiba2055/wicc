from rest_framework import serializers 

from .models import ParkingRecord, TicketCounter

class TicketCounterSerializer(serializers.ModelSerializer):

    class Meta:
        model = TicketCounter
        fields = ('ticket_seller_name',)

class ParkingRecordSerialiser(serializers.ModelSerializer):

    class Meta:
        model = ParkingRecord
        fields = ('ticket_counter', 'driver_name', 'vehicle_name', 'arrival_time', 'departure_time', 'paid')
