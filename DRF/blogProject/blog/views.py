from django.shortcuts import render

from rest_framework.generics import CreateAPIView, ListAPIView,RetrieveAPIView, UpdateAPIView, DestroyAPIView
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated

from .models import BlogPost, Comment 
from .serializers import BlogPostSerializer, CommentSerializer
# Create your views here.

class BlogPostCreateView(CreateAPIView):

    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer


class BlogPostListView(ListAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer
    
class BlogPostUpdateView(UpdateAPIView):
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer
    
class BlogPostRetrieveView(RetrieveAPIView):
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer
    
class BlogPostDestroyView(CreateAPIView):
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer
    


class CommentCreateView(CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

class CommentListView(ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    
class CommentUpdateView(UpdateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    
class CommentRetrieveView(RetrieveAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    
class CommentDestroyView(CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    
