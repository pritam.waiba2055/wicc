from rest_framework import serializers 

from .models import BlogPost, Comment 

class BlogPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = BlogPost
        fields = ['user', 'title', 'content','slug', 'created', 'updated']

class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment 
        fields = ['post', 'user', 'content'] 

 