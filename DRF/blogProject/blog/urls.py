from django.urls import path 

from . import views 

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [ 
    path('', views.BlogPostListView.as_view(), name='post_list'),
    path('create/', views.BlogPostCreateView.as_view(), name='post_create'),
    path('<int:pk>/', views.BlogPostRetrieveView.as_view(), name='post_retrieve'),
    path('<int:pk>/update/', views.BlogPostUpdateView.as_view(),name='post_update'),
    path('<int:pk>/destory/', views.BlogPostDestroyView.as_view(), name='post_destroy'), 

    path('comment/', views.CommentListView.as_view(), name='comment_list'),
    path('comment/create/', views.CommentCreateView.as_view(), name='comment_create'),
    path('comment/<int:pk>/', views.CommentRetrieveView.as_view(), name='comment_retrieve'),
    path('comment/<int:pk>/update/', views.CommentUpdateView.as_view(),name='comment_update'),
    path('comment/<int:pk>/destory/', views.CommentDestroyView.as_view(), name='comment_destroy'), 

 
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

]