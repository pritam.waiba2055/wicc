from django.core.mail import send_mail
from django.conf import settings 
from django.urls import reverse
from datetime import datetime 

def send_password_reset_mail(email, message, subject):
    send_mail(
        subject,
        message,
        settings.EMAIL_HOST_USER,
        [email,],
        fail_silently=False,
    )

''' OTP part '''


